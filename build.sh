#!/bin/bash

VERS="0.51";
FILE="2s2d-ge-$VERS.min.js";

echo -e "\nBuilding 2s2d-ge version $VERS\n"

uglifyjs -o js/${FILE} -c -m -- lib/jqless.js \
    src/surface.class.js \
    src/toolbox.class.js \
    src/primitives.class.js \
    src/touchdevice.class.js \
    src/scene.class.js \
    src/tiles.class.js \
    src/sprite.class.js

echo -e "Build done \n "
