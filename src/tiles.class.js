// Module: Graphic background tiles handler
// Version: 1.0
// ES: 3
// Created: xxxx-xx-xx
// Updated: 2020-03-23

/*
 * Updates
 * 20200323: Various standard complience fixes
 * 20200225: Minor code identation fix to improve readability
 * 20180610: ??
 * 
 * 
 */

var Tiles = function()
{
    this._status = 0;
    this._tools = new Toolbox();
    var _instance = this;
	
//--------------------------------------------------------------------//
	
    this.loadImage = function(src)
    {
        this._status++;
        this._tools.loadImage(src, this._doneLoad);
    };

//--------------------------------------------------------------------//
	
    this._doneLoad = function(img)
    {
        _instance._sprite = img;
        _instance._status--;
    };
	
//--------------------------------------------------------------------//	

    this.loadTileset = function(w, h)
    {
        
    };
    
//--------------------------------------------------------------------//

    this.status = function()
    {
        return this._status;
    };

};
