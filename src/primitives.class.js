// Module: Graphic primitives
// Version: 2.2
// ES: 3
// Created: xxxx-xx-xx
// Updated: 2020-03-23

/*
 * 20200323: Comments translation to english and various standard complience fixes
 * 20200319: Added line_box method to draw hollow rectangles
 * 20180610: ???
 */
 
var Primitives = Surface;

////////////////////////////////////////////////////////////////////////////////////////////////
//                                TEXT PROCESSING ROUTINES                                    //
////////////////////////////////////////////////////////////////////////////////////////////////

Primitives.prototype = 
{
// ------------------------------------------------------------------ //
// Method to convert from degrees to radians
    deg2rad :  Toolbox.deg2rad,
    
    // Prototype that draws a text on screen
    _text : function(font, color, x, y, text, bold, outline)
    {
        this._surface.context.strokeStyle = color;
        this._surface.context.fillStyle   = color;
        this._surface.context.font        = font;
        if( outline === false ) this._surface.context.fillText( text, x, y );
        if( bold    === true  ) this._surface.context.strokeText( text, x, y );
    },

    // Draws normal or bold text (by font)
    text : function( font, color, x, y, text )
    {
        this._text( font, color, x, y, text, false, false );
    },

    // Draws text with outline to make it bold (by canvas)
    // If bold is passed in font then you get hyperbold
    text_bold : function( font, color, x, y, text )
    {
        this._text( font, color, x, y, text, true, false);
    },

    // Draws only the outline of a text
    text_outline : function( font, color, x, y, text )
    {
        this._text( font, color, x, y, text, true, true );
    },

    // Draws a text and a shadow at offset ofsx, ofsy
    text_shadowed : function( font, color, x, y, text, ofsx, ofsy, scolor )
    {
        if (typeof scolor === "undefined") {
            scolor = "#000";
        }
        this._text( font, scolor, ( x + ofsx ), ( y + ofsy ), text, false, false );
        this._text( font, color, x, y, text, false, false );
    },

    // Gets width and height of a rendered text
    textSize : function( font, text )
    {
        var c = document.createElement( 'canvas' );
        var ctx = c.getContext("2d");

        ctx.font = font;

        var fs = [ ctx.measureText(text).width, parseInt(ctx.font) ];
        ctx = c = undefined;

        return fs;
    },

////////////////////////////////////////////////////////////////////////////////////////////////
//                                  CANVAS RENDER ROUTINES                                    //
////////////////////////////////////////////////////////////////////////////////////////////////

    // Methd returning a two steps gradient
    gradient : function( x1, y1, x2, y2, color1, color2 )
    {
        var gradient = this._surface.context.createLinearGradient( x1, y1, x2, y2 );
        gradient.addColorStop(0, color1);
        gradient.addColorStop(1, color2);
        return gradient;
    },

    // Draws a line of specified color between the points given by param
    line : function( x1, y1, x2, y2, color )
    {
        this._surface.context.strokeStyle = color;
        this._surface.context.beginPath();
        this._surface.context.moveTo( x1, y1 );
        this._surface.context.lineTo( x2, y2 );
        this._surface.context.stroke();
    },

    // Method that draws a hollow rectangle
    // The color can be replaced by a gradient
    line_box : function( x, y, width, height, color ) 
    {
        this._surface.context.strokeStyle = color;
        this._surface.context.rect( x, y, width, height );
        this._surface.context.stroke();
    },
    
    // Draws a line with a gradient
    line_gradient : function( x1, y1, x2, y2, color1, color2 )
    {
        var grad = this.gradient( x1, y1, x2, y2, color1, color2 );
        this.line(x1, y1, x2, y2, grad );
    },

    // Method to draw a filled rectangle on canvas
    // The color can be replaced with a gradient
    line_fill : function( x, y, width, height, color ) 
    {
        this._surface.context.fillStyle = color;
        this._surface.context.fillRect( x, y, width, height );
    },

    // Method to draw a square on canvas
    pset : function( x, y, side, color ) 
    {
        this._surface.context.fillStyle = color;
        this._surface.context.fillRect( x, y, side, side );
    },

    // Arc prototype to draw open and closed arcs as well as circles
    _arc : function( x, y, radio, degini, degfin, color, closed, fill, lineWidth)
    {
        if (typeof lineWidth === 'undefined' || ! lineWidth) {
            lineWidth = 1;
        }

        this._surface.context.lineWidth = lineWidth;
        this._surface.context.fillStyle   = color;
        this._surface.context.strokeStyle = color;
        this._surface.context.beginPath();
        radini = this.deg2rad( degini );
        radfin = this.deg2rad( degfin );
        this._surface.context.arc( x, y, radio, radini, radfin, true ); 
        if( closed === true ) this._surface.context.closePath();
        if( fill   === true ) this._surface.context.fill();
        this._surface.context.stroke();
        this._surface.context.lineWidth = 1;
    },

    // Self explantory
    circle : function(x, y, radio, color, fill, lineWidth )
    {
        if( ! fill ) fill = false;
        this._arc( x, y, radio, 0, 360, color, true, fill, lineWidth );
    },

    // Open and hollow arc
    arc : function(x, y, radio, degini, degfin, color, lineWidth )
    {
        this._arc(x, y, radio, degini, degfin, color, false, false, lineWidth );
    },

    // Close and hollow arc
    arc_close : function( x, y, radio, degini, degfin, color, lineWidth )
    {
        this._arc( x, y, radio, degini, degfin, color, true, false, lineWidth );
    },

    // Closed and solid arc
    arc_fill : function( x, y, radio, degini, degfin, color, lineWidth )
    {
        this._arc( x, y, radio, degini, degfin, color, true, true, lineWidth );
    },

    // Method to draw irregular polygons
    // This method allows open, closed, filled and gradient polygons
    _poly : function( vertex, color, closed, fill )
    {
        this._surface.context.strokeStyle = color;
        this._surface.context.fillStyle   = color;
        this._surface.context.beginPath();
        var start = true;
        for ( var v in vertex ) {
            var k = vertex[v].split(',');
            if(start === true){
                this._surface.context.moveTo( k[ 0 ], k[ 1 ] );
            } else {
                this._surface.context.lineTo( k[ 0 ], k[ 1 ] );
            }
            start = false;
        }
        if( closed === true ) this._surface.context.closePath();
        if( fill   === true ) this._surface.context.fill();
        ctx.stroke();
    },

    // Open polygon
    poly : function( vertex, color )
    {
        this._poly( vertex, color, false, false );
    },

    // Hollow closed polygon
    poly_closed : function(vertex, color )
    {
        this._poly( vertex, color, true, false );
    },

    // Solid polygon
    poly_fill : function(vertex, color )
    {
        this._poly( vertex, color, true, true );
    }

};
