// Module: Graphic base surface object handler
// Version: 2.1
// ES: 3
// Created: xxxx-xx-xx
// Updated: 2020-06-19

/*
 * Updates
 * 20200619: New method getId returns the id of the associated canvas
 * 20200618: Added method RealWidth and RealHeight to set and know the real 
 *           dimensions of the surface
 * 20200529: Fixed bug if flip method
 * 20200528: Added methods to flip surface horizontally and vertically
 * 20200323: Various standard complience fixes
 * 20200225: Minor code identation fix to improve readability
 * 20180610: ??
 * 
 * 
 */

var Surface = function( Width, Height )
{
    var _surface;
    var _width, _height;
    
//--------------------------------------------------------------------//
    this.create = function( Width, Height )
    {
        var serial = new Date();
            serial = (serial.getTime() % 10000) + Math.floor(Math.random() * 100);
            
        var canvas = document.createElement('canvas');
            canvas.id     = "idLayer_" + serial;
            canvas.width  = Width;
            canvas.height = Height;
            canvas.style.top      = 0;
            canvas.style.left     = 0;
            canvas.style.zIndex   = 8;
            canvas.style.position = "absolute";
        var context = canvas.getContext( "2d" );
        
        this._surface = { 'surface' : canvas, 'context' : context };
        
        this._width = 0;
        this._height = 0;
    };

//--------------------------------------------------------------------//
    this.getId = function()
    {
        return $(this.getSurface()).attr("id");
    };
    
//--------------------------------------------------------------------//
    this.getSurface = function()
    {
        return this._surface.surface;
    };
    
//--------------------------------------------------------------------//
    this.getContext = function()
    {
        return this._surface.context;
    };
        
//--------------------------------------------------------------------//
    this.Width = function(Width)
    {
        if (typeof Width === "undefined") {
            return this._surface.surface.width;
        } else {
            this._surface.surface.width = Width;
        }
    };

//--------------------------------------------------------------------//
    this.RealWidth = function(Width)
    {
        if (typeof Width === "undefined") {
            return parseInt(this._width);
        } else {
            this._width = Width;
        }
    };

//--------------------------------------------------------------------//
    this.Height = function(Height)
    {
        if (typeof Height === "undefined") {
            return this._surface.surface.height;
        } else {
            this._surface.surface.height = Height;
        }
    };

//--------------------------------------------------------------------//
    this.RealHeight = function(Height)
    {
        if (typeof Height === "undefined") {
            return parseInt(this._height);
        } else {
            this._height = Height;
        }
    };

//--------------------------------------------------------------------//
    this.Position = function(Position)
    {
        if (typeof Position === "undefined") {
            return this._surface.surface.style.position;
        } else {
            this._surface.surface.style.position = Position;
        }
    };

//--------------------------------------------------------------------//
    this.Top = function(Top)
    {
        if (typeof Top === "undefined") {
            return this._surface.surface.style.top;
        } else {
            this._surface.surface.style.top = Top;
        }
    };

//--------------------------------------------------------------------//
    this.Left = function(Left)
    {
        if (typeof Left === "undefined") {
            return this._surface.surface.style.left;
        } else {
            this._surface.surface.style.left = Left;
        }
    };
    
// ------------------------------------------------------------------ //
    this.clear = function(x, y, width, height)
    {
        this._surface.context.clearRect( x, y, width, height );
    };

//--------------------------------------------------------------------//
    this.clearAll = function()
    {
        var s = this._surface.surface;
        this._surface.context.clearRect( 0, 0, s.width, s.height );
    };
    
//--------------------------------------------------------------------//
    this.flip = function(image, axis)
    {
        this._surface.context.save(); // Save the current state
        var posx = 0, posy = 0;
        
        switch(axis){
            case 'y' :
                this._surface.context.scale(1, -1);
                posy = image.height + -1;
                break;
            default :
                this._surface.context.scale(-1, 1);
                posx = image.width * -1;
                break;
        }
        
        this._surface.context.drawImage(image, posx, posy);
        this._surface.context.restore();
    };
  
//--------------------------------------------------------------------//
    this.flipHorizontal = function(image) { this.flip(image); };
    
//--------------------------------------------------------------------//
    this.flipVertical = function(image) { this.flip(image, 'y'); };
    
//--------------------------------------------------------------------//
    this.drawImageRect = function( canOrigin, intOrigLeft, intOrigTop, intOrigWidth, intOrigHeight, intX, intY )
    {
        this._surface.context.drawImage(
            canOrigin, 
            intOrigLeft, 
            intOrigTop, 
            intOrigWidth, 
            intOrigHeight, 
            intX, 
            intY,
            intOrigWidth, 
            intOrigHeight );
    };
    
//--------------------------------------------------------------------//
    this.drawImage = function( canOrigin, intX, intY, intW, intH )
    {
        if (typeof intW === "undefined" && typeof intH === "undefined") {
            this._surface.context.drawImage( canOrigin, intX, intY );
        } else {
            this._surface.context.drawImage( canOrigin, intX, intY, intW, intH );
        }
    };
    
//--------------------------------------------------------------------//
    this.rotate = function(degrees)
    {
        var ctx = this._surface.context;
        var surface = this._surface.surface;

        ctx.save();
        ctx.translate(surface.width / 2, surface.height / 2);
        ctx.rotate(Toolbox.deg2rad(degrees));
        ctx.translate(surface.width / -2, surface.height / -2);
    };
    
//--------------------------------------------------------------------//
//--------------------------------------------------------------------//
    this.create( Width, Height );
};
