// Module: Virtual stick driver
// Version: 1.3
// ES: 3
// Created: xxxx-xx-xx
// Updated: 2020-05-13

/*
 * Updates
 * 20200513: Added full transparent gamepad support, you can change the 
 *           mapping or adjust the analog deadzone and call to the Config 
 *           method to update.
 * 20200508: Fixed bug in stick position precalc index when ox or oy < ow 
 * 20200427: When in portrait, stick area gets a 50% width screen area to improve control
 * 20200323: Various standard complience fixes
 * 20200307: Fixed analog stick positioning and redraw clearing bugs
 * 20200208: Self center stick on release
 * 
 * 
 */
 
var touchDeviceInstance;

/**
 * target: id del div objetivo a usar como contenedor del control
 * mode: 0: 2 botones y acelerómetro
 * 		 1: análogo y botón
 * 		 2: análogo y 2 botones
 *       3: análogo y 3 botones
 * 		 4: análogo, 2 botones y análogo con selector de botones configurable
 **/
function TouchDevice(target, mode, deadzone, invert) 
{
    this._buttons = {};
    this._stick = {};
    this._buttonC = {};
    this._tilt = {};

    this._divButtonA = false;
    this._divButtonB = false;
    this._divButtonC = false;
    this._divButtonM = false;
    this._divStick = false;
    this._surface = false;
    this._canvasStick = false;
    this._canvasStick2 = false;
    this._invert = false;
    this._deadzone = 0;
    this._tiltCalibration = 0;
    this._mode = 0;
    this._stickColor = "rgba(0,0,0,.5)";
    /* Virtual buttons A, B, C & P
     * On a 360 type gamepad these are buttons A, B, X & Start
     */
    this._gamepadBindings;
    // Vertical and horizontal axis respectivelly
    this._gamepadAxes;
    // Buttons for digital pad, up, down left & right
    this._gamepadDigital;
    this._gamepadDeadzone;
    this._lastGamepad = -1;
    this.debugMode = 0;
	
	this._precalc = {
		modulus : []
	};
	
//----------------------------------------------------------------------
    this.stickReset = function() 
    {
        this._stick = {
            act : false,// Indicador de activo
            x   : 0,	// Lectura inicial del cursor x (touchstart)
            y   : 0,	// Lectura inicial del cursor y (touchstart)
            xd  : 0,	// Lectura inicial del cursor x (touchmove)
            yd  : 0,	// Lectura inicial del cursor y (touchmove)
            xv  : 0,	// Diferencia entre lectura actual y anterior x
            yv  : 0,	// Diferencia entre lectura actual y anterior y
            xr  : 0,	// Valor x módulo 32 (-32 a 32)
            yr  : 0,    // Valor y módulo 32 (-32 a 32)
            ow  : 0,	// Ancho exterior (Outer width of the stick)
            ow2 : 0,	// Cuadrado de o2 para calculo de vector
            owb : 0,	// LineWidth for the inner and outer circles of the stick
            iw  : 0,	// Inner width of the stick
            dt  : 0,	// datetime de iniciado el evento
            id  : 0,	// id del objeto
            ox	: 0,
            oy	: 0
        };

        var dz = typeof this._deadzone === 'undefined' ? 0 : this._deadzone;
        this._tilt = {
            active   : false,
            deadzone : dz,
            val      : 0
        };
    };

//----------------------------------------------------------------------

    this.buttonsReset = function()
    {
        this._buttons = {
            a  : 0,
            b  : 0,
            c  : 0,
            m  : 0,
            at : 0,
            bt : 0,
            ct : 0,
            mt : 0
        };
    };

//----------------------------------------------------------------------

    this.getStick = function()
    {
        var ret = {
            touch  : true,
            active : this._stick.act,
            x      : this._stick.xr, 
            y	   : this._stick.yr
        };
        
        if(this._lastGamepad >= 0) {
            var gp = navigator.getGamepads()[touchDeviceInstance._lastGamepad];
            var gamePad = {
                'touch'  : false,
                'active' : true,
                'x'      : Math.round(gp.axes[0] * 32),
                'y'      : Math.round(gp.axes[1] * 32)
            };
            if(Math.abs(gamePad.x) <= this._gamepadDeadzone) gamePad.x = 0;
            if(Math.abs(gamePad.y) <= this._gamepadDeadzone) gamePad.y = 0;

            if(gamePad.x === 0) {
                gamePad.x = gp.buttons[this._gamepadDigital[2]].pressed ? -32 : (gp.buttons[this._gamepadDigital[3]].pressed ? 32 : 0);
            }

            if(gamePad.y === 0) {
                gamePad.y = gp.buttons[this._gamepadDigital[0]].pressed ? -32 : (gp.buttons[this._gamepadDigital[1]].pressed ? 32 : 0);
            }

            if((Math.abs(gamePad.x) > Math.abs(ret.x)) || (Math.abs(gamePad.y) > Math.abs(ret.y))) {
                ret = gamePad;
            }
        } else {
            if(Math.abs(ret.x) <= this._gamepadDeadzone) ret.x = 0;
            if(Math.abs(ret.y) <= this._gamepadDeadzone) ret.y = 0;
        }

        return ret;
    };

//----------------------------------------------------------------------

	this.getGamepad = function()
	{
            if(touchDeviceInstance._lastGamepad === -1) return;
            console.log('dd: ' + touchDeviceInstance._lastGamepad);
            var gp = navigator.getGamepads()[touchDeviceInstance._lastGamepad];
            console.table(gp.buttons);
            //~ console.log(gp);
	};
	
//----------------------------------------------------------------------
	
    this.getButtons = function()
    { 
        var ret = this._buttons;
        if(this._lastGamepad >= 0) {
            var gp = navigator.getGamepads()[touchDeviceInstance._lastGamepad];
            var gamePad = {
                'a' : gp.buttons[this._gamepadBindings[0]].pressed,
                'b' : gp.buttons[this._gamepadBindings[1]].pressed,
                'c' : gp.buttons[this._gamepadBindings[2]].pressed,
                'm' : gp.buttons[this._gamepadBindings[3]].pressed
            };
            ret = { "virt" : ret, "gpad" : gamePad};
        }
        return ret;
    };

//----------------------------------------------------------------------

    this.calibrateTilt = function()
    {
        this._tiltCalibration = this._tilt.val;
        $("#txtDebug").html(this._tiltCalibration);
    };

//----------------------------------------------------------------------

    this.setTiltCalibration = function(val)
    {
        this._tiltCalibration = val;
    };
	
//----------------------------------------------------------------------

    this.getTilt = function()
    {
        if (this._tilt.active === true) {
            return Math.abs(this._tilt.val) <= this._tilt.deadzone ? 0 : this._tilt.val;
        } else {
            return false;
        }
    };
	
//----------------------------------------------------------------------

    this.getButtonPress = function(button)
    {
        var ret = this._buttons[button] === 1 || this._buttons[button] === 3 ? 1 : 0;
        if (this._buttons[button] === 1) {
            var dt = (Date.now() - this._buttons[button + 't']);
            if (dt > 101) {
                this._buttons[button] = 2;
            } else {
                ret = 99;
                //~ ret = 1;
            }
        } 
        if(this._lastGamepad >= 0) {
            var buttons = this.getButtons();
            ret = buttons.gpad[button] === true ? 1 : 0;
            //~ console.table(buttons.gpad);
        }
        if (this._buttons[button] === 3) this._buttons[button] = 0;
        return ret;
    };

//----------------------------------------------------------------------
	
    this.getButtonHold = function(button)
    {
        return this._buttons[button] === 2 ? 1 : 0;
    };
	
//----------------------------------------------------------------------

    this.stickRecalc = function(e)
    {
        var touch = e.touches[touchDeviceInstance._stick.id];
        this._stick.xd = touch.pageX;
        this._stick.yd = touch.pageY;

        this._stick.xv = (this._stick.xd - this._stick.x);
        this._stick.yv = (this._stick.yd - this._stick.y);
        var s = this._stick;

        var xvs = s.xv < 0 ? -1 : 1;
        var yvs = s.yv < 0 ? -1 : 1;
		
        this._stick.xr = s.xv > s.ow ? 32 : s.xv < s.ow * -1 ? - 32 : this._precalc.modulus[Math.abs(Math.floor(s.xv))] * xvs;
        this._stick.yr = s.yv > s.ow ? 32 : s.yv < s.ow * -1 ? -32 : this._precalc.modulus[Math.abs(Math.floor(s.yv))] * yvs;
    };

//----------------------------------------------------------------------

	this.setStickColor = function(col)
	{
            this._stickColor = col;
	};
	
//----------------------------------------------------------------------

    this.stickDraw = function()
    {
        if(this._lastGamepad >= 0) return;
        var s = this._stick;
        var vector = (s.xv * s.xv) + (s.yv * s.yv);

        if (vector > s.ow2) {
            var a = Math.atan(Math.abs(s.yv) / Math.abs(s.xv));
            var ny = Math.round(Math.sin(a) * s.ow);
            var nx = Math.round(Math.cos(a) * s.ow);

            nx = s.xv > 0 ? nx : nx * -1;
            ny = s.yv > 0 ? ny : ny * -1;

            s.x += s.xv - nx;
            s.y += s.yv - ny;

            s.xv = (s.xd - s.x);
            s.yv = (s.yd - s.y);
        }

        this._canvasStick.clear(s.ox - (s.s >> 1), s.oy - (s.s >> 1), s.s, s.s);
        this._canvasStick.circle(s.x, s.y, s.ow, this._stickColor, false, s.owb);
        this._canvasStick.circle(s.xd, s.yd, s.iw, this._stickColor, true);
        s.ox = s.x;
        s.oy = s.y;
    };

//----------------------------------------------------------------------

    this.buttonC_draw = function(buttons)
    {
        if (buttons > 8) {
            buttons = 8;
        }
        var center = {
            x : this._canvasStick2.getSurface().width / 2,
            y : this._canvasStick2.getSurface().height / 2
        };

        var labels = ["A", "B", "C", "D", "E", "F", "G", "H"];
        //Font between 2em for 1 button and 3em for 8 buttons
        var font = (3 - buttons / 8) + "em Helvetica, Arial";
        var seg_an = (360 / buttons) - 1;
        var ow = this._stick.ow ;
        var owb = this._stick.owb << 2;
        var j = 270 - (seg_an / 2);

        for (var i = 0; i < buttons; i++) {
            this._canvasStick2.arc(center.x, center.y, ow, j, j - seg_an, "rgba(0,0,0,.5)", owb);
            j = j - seg_an - 1;
        }

        var scale = 2.8;
        var bcenter = ow * (scale / 2);
        var obwm = owb / 2;

        var cButton = new Primitives( ow * scale, ow * scale);

        for (var i = 0; i < buttons; i++) {
            var fs = cButton.textSize(font, labels[i]);
            var vofs = ow / 6;
            cButton.clearAll();
            cButton.text_bold(font, "rgba(255,255,255,.5)", bcenter - fs[0] / 2, bcenter - ow + vofs, labels[i]);
            this._canvasStick2.drawImage(cButton.getSurface(), center.x - bcenter, center.y - bcenter);
            cButton.rotate(seg_an);
        }
        cButton.getContext().restore();
    };

//----------------------------------------------------------------------

    this.destroy = function()
    {
        var root = $(target).getDOM();
        var children = root.childNodes;
        for (var i = children.length - 1; i >= 0 ; i--) {
            root.removeChild(children[i]);
        }
    };

//----------------------------------------------------------------------

    this.init = function()
    {
        this.stickReset();
        this.buttonsReset();

        var baseCss = {
            'position' : 'absolute', 
            'top'      : '0px',
            'left'     : '0px',
            'height'   : '100%',
            'width'    : '33%'
        };
        
        var buttonA1css = {'right' : '0px', 'left' : 'auto'};
        var buttonA2css = {'height' : '50%', 'bottom' : '0px', 'top' : 'auto'};
        var buttonBcss = {'right' : '0px', 'left' : 'auto', 'height' : '50%'};
        var buttonCcss = {'right' : 'auto', 'left' : '0px', 'height' : '50%'};
        
        if (this._invert) {
            baseCss.left = 'auto';
            baseCss.right = '0px';
            buttonA1css.right = 'auto';
            buttonA1css.left = '0px';
            buttonBcss.right = 'auto';
            buttonBcss.left = '0px';
            buttonCcss.right = '0px';
            buttonCcss.left = 'auto';
        }
        $(target).css(baseCss);
        $(target).css({"width" : "100%"});

        // Create button A in DOM
        this._divButtonA = $.create('div');
        this._divButtonA.id = 'divButtonA';
        $(this._divButtonA).css(baseCss);
        $(this._divButtonA).css(buttonA1css);

        // If in a mode that requires it, then create button B
        if (this._mode !== 1) {
            this._divButtonB = $.create('div');
            this._divButtonB.id = 'divButtonB';
            $(this._divButtonB).css(baseCss);
        }

        // If in mode greater or equal to 2 (2 buttons & stick, adjust size of button A and button B
        if (this._mode >= 2) {
            $(this._divButtonA).css(buttonA2css);
            $(this._divButtonB).css(buttonBcss);
            if (this._mode === 3) {
                this._divButtonC = $.create('div');
                this._divButtonC.id = 'divButtonC';
                $(this._divButtonC).css(baseCss);
                $(this._divButtonC).css(buttonCcss);
            } 
        }

        // Creates button M, recommended use, back to menu
        this._divButtonM = $.create('div');
        this._divButtonM.id = 'divButtonM';
        $(this._divButtonM).css(baseCss);
        $(this._divButtonM).css({'height' : '45%', 'width' : '32%', 'left' : '34%'});

        var width = window.innerWidth;
        var height  = window.innerHeight;

        this._stick.ow = Math.round(width / 12); // Outer width of the stick
        this._stick.ow2 = this._stick.ow * this._stick.ow; // Precalc square of ow for vector calculus purposes
        this._stick.iw = Math.round(this._stick.ow / 3); // Inner width of the stick
        this._stick.owb = Math.round(this._stick.ow / 6); // LineWidth for the inner and outer circles of the stick
        // Length of side of square rounding the stick needed to erase it from screen
        this._stick.s = this._stick.iw * 2 + this._stick.ow * 2; 

        for (var i = 0; i <= this._stick.ow; i++) this._precalc.modulus.push(Math.floor(i * 32 / this._stick.ow));

        // If in modes > 0 then create the stick sensor area and the canvas to draw the control
        if (this._mode > 0) {
            this._divStick = $.create('div');
            this._divStick.id = 'divStick';
            $(this._divStick).css(baseCss);
            this._canvasStick = new Primitives( width, height );
            var surfaceStick = this._canvasStick.getSurface();
            
            if(window.screen.width < window.screen.height) {
                $(this._divStick).css({'width' : '50%'});
            }
            
            $(surfaceStick).css({"z-index" : "-1"});
            if (this._mode >= 3) {
                $(this._divStick).css({'height' : '50%', 'bottom' : '0px', 'top' : 'auto'});
            }
        }

        // If in mode 4 create the stick sensor area for the C button and shrink the
        // area for the stick
        if (this._mode === 4) {
            this._divStick2 = $.create('div');
            this._divStick2.id = 'divStick2';
            $(this._divStick2).css(baseCss);
            this._canvasStick2 = new Primitives(width, height);
            var surfaceStick2 = this._canvasStick2.getSurface();

            this.buttonC_draw(5);

            $(surfaceStick2).css({"z-index" : "-1"});
            $(this._divStick2).css({'height' : '50%'});
        }

        // Attach created controls to the target div
        $(target).append(this._divButtonA);
        $(target).append(this._divButtonM);
        if (this._mode !== 1) {
            $(target).append(this._divButtonB);
        }
        if (this._mode > 0) {
            $(target).append(this._divStick);
            $(target).append(surfaceStick);
        }
        if (this._mode === 3) {
            $(target).append(this._divButtonC);
        }
        if (this._mode === 4) {
            $(target).append(surfaceStick2);
        }

        // Show debug control values
        if (this.debugMode === 1) {
            console.log(this._divButtonA);
            if (this._mode === 0 || this._mode === 2) console.log(this._divButtonB);
            console.log(this._divButtonM);
            if (this._mode === 1 || this._mode === 2) {
                console.log(this._divStick);
                console.log(this._canvasStick.getSurface());
            }
        }
    };

//----------------------------------------------------------------------

	this.Config = function()
	{
            var defConfig = {
                buttons  : [0, 1, 2, 9],
                axes     : [0, 1],
                digital  : [12, 13, 14, 15],
                deadzone : 8
            };
            var joyConf = $.tb.conf('fw', defConfig);

			if(typeof joyConf.deadzone === 'undefined') $.tb.conf('fw', defConfig, defConfig);
			
            /* Virtual buttons A, B, C & P
             * On a 360 type gamepad these are buttons A, B, X & Start
             */
            this._gamepadBindings = joyConf.buttons;
            // Vertical and horizontal axis respectivelly
            this._gamepadAxes = joyConf.axes;
            // Buttons for digital pad, up, down left & right
            this._gamepadDigital = joyConf.digital;
            this._gamepadDeadzone = joyConf.deadzone;
	};

//----------------------------------------------------------------------

    this.bindControls = function()
    {
        // ---- Gamepad handling
        $(window).on('gamepadconnected', function(e) {
            var gp = navigator.getGamepads()[e.gamepad.index];
            touchDeviceInstance._lastGamepad = e.gamepad.index;
            console.log("A gamepad connected:");
            //console.log(e.gamepad);
        });

        $(window).on('gamepaddisconnected', function(e) {
            var gp = navigator.getGamepads()[e.gamepad.index];
            if(gp === touchDeviceInstance._lastGamepad) {
                touchDeviceInstance._lastGamepad = -1;
                console.log('Gamepad disconnected');
            }
        });

        // ---- /Gamepad handling
		
        // ---- Button A 
        $(this._divButtonA).on('touchstart', function(event) {
            event.preventDefault();
            touchDeviceInstance._buttons.a = 1;
            touchDeviceInstance._buttons.at = Date.now();
        });

        $(this._divButtonA).on('touchend', function(event) {
            event.preventDefault();
            touchDeviceInstance._buttons.a = touchDeviceInstance._buttons.a === 2 ? 0 : 3;
        });
        // ---- /Button A

        // ---- Button B
        if (this._mode !== 1) {
            $(this._divButtonB).on('touchstart', function(event) {
                event.preventDefault();
                touchDeviceInstance._buttons.b = 1;
                touchDeviceInstance._buttons.bt = Date.now();
            });

            $(this._divButtonB).on('touchend', function(event) {
                event.preventDefault();
                touchDeviceInstance._buttons.b = touchDeviceInstance._buttons.b === 2 ? 0 : 3;
            });
        }
        // ---- /Button B

        // ---- Button M
        $(this._divButtonM).on('touchstart', function(event) {
            event.preventDefault();
            touchDeviceInstance._buttons.m = 1;
        });

        $(this._divButtonM).on('touchend', function(event) {
            event.preventDefault();
            touchDeviceInstance._buttons.m = touchDeviceInstance._buttons.m === 2 ? 0 : 3;
            touchDeviceInstance._buttons.mt = Date.now();
        });
        // ---- /Button M

        // ---- /Button C
        if (this._mode === 3) {
            $(this._divButtonC).on('touchstart', function(event) {
                event.preventDefault();
                touchDeviceInstance._buttons.c = 1;
            });

            $(this._divButtonC).on('touchend', function(event) {
                event.preventDefault();
                touchDeviceInstance._buttons.c = touchDeviceInstance._buttons.c === 2 ? 0 : 3;
                touchDeviceInstance._buttons.ct = Date.now();
            });
        }
        // ---- /Button C

        // ---- Analog Stick
        if (this._mode > 0) {
            $(this._divStick).on('touchstart', function(event) {
                var idEvent = 0;
                event.preventDefault();
                if (event.touches.length > 1) { 
                    var ret = 0;
                    for (var i = 0; i < event.touches.length; i++) {
                        if (event.touches[i].target.id === 'divStick') {
                            ret++;
                            idEvent = i;
                        }
                    }
                    if (ret > 1) return;
                }

                touchDeviceInstance._stick.id = idEvent;

                var touch = event.touches[touchDeviceInstance._stick.id];
                touchDeviceInstance._stick.x = touch.pageX;
                touchDeviceInstance._stick.y = touch.pageY;
                touchDeviceInstance._stick.dt = Date.now();
                touchDeviceInstance._stick.act = true;

                touchDeviceInstance.stickRecalc(event, idEvent);
                touchDeviceInstance.stickDraw();
            });

            $(this._divStick).on('touchmove', function(event) {
                event.preventDefault();
                touchDeviceInstance.stickRecalc(event);
            });

            $(this._divStick).on('touchend', function(event) {
                event.preventDefault();
                touchDeviceInstance._canvasStick.clearAll();
                touchDeviceInstance._stick.xr = 0;
                touchDeviceInstance._stick.yr = 0;
                touchDeviceInstance._stick.act = false;
            });
        } // ---- /Analog Stick

        // ---- Tilt
        if (this._mode === 0) {
            this._tilt.active = true;
            $(window).on('devicemotion', function(event) {
                event.preventDefault();
                var acceleration = event.accelerationIncludingGravity;
                touchDeviceInstance._tilt.val = Math.round(acceleration.y * 10 - touchDeviceInstance._tiltCalibration);
            });
        } // ---- /Tilt
    };
	
//----------------------------------------------------------------------

//----------------------------------------------------------------------

    if (! target || typeof mode === 'undefined') {
        console.log("ERROR: Fail to initialize TouchDevice class, no target id or touch mode declared");
        console.log("ej: var t = new TouchDevice('target', 0);");
        return;
    }

    if (typeof $ === 'undefined') {
        console.log("ERROR: jqless class must be instanced before TouchDevice inits");
        return;
    }

    if (typeof Primitives === 'undefined') {
        console.log("ERROR: Class Primitives must be loaded before than TouchDevice");
        return;
    }

    touchDeviceInstance = this;

    this._mode = mode;
    this._deadzone = deadzone;
    this._invert = invert;

    target = "#" + target;
	
    this.Config();
    this.init();
    this.bindControls();
}
