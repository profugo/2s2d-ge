// Module: Sprite handler
// Version: 1.0
// ES: 3
// Created: xxxx-xx-xx
// Updated: 2020-06-16

/*
 * Updates
 * 20200616: Added method getLoadStatus for compatibility
 * 20200529: Added methods to flip frames
 * 20200323: Various standard complience fixes
 * 20200225: Minor code identation fix to improve readability
 * 20180610: ??
 * 
 * 
 */

var Sprite = function()
{
    var _instance = this;
    this._tools = new Toolbox();
    this._status = 0;
    this._sprite;
    this._frames = [];
    this._frame = 0;

//--------------------------------------------------------------------//	

    this.status = function()
    {
        return this._status;
    };

//--------------------------------------------------------------------//

    this.getLoadStatus = function()
    {
        return this._status;
    };
    
//--------------------------------------------------------------------//

    this.image = function(img)
    {
        if (typeof img === "undefined") {
            return this._sprite;
        } else {
            this._sprite = img;
        }
    };
	
//--------------------------------------------------------------------//
	
    this.loadImage = function(src)
    {
        this._status += 1;
        this._tools.loadImage(src, this._doneLoad);
    };

//--------------------------------------------------------------------//
	
    this._doneLoad = function(img)
    {
        _instance._sprite = img;
        _instance._status -= 1;
    };

//--------------------------------------------------------------------//
	
    this.get = function()
    {
        var ret = this._frames[this._frame];
        this._frame++;
        if (this._frame >= this._frames.length) { this._frame = 0; }
        return ret;
    };

//--------------------------------------------------------------------//
	
    this.getTile = function(x, y, w, h)
    {
        var _spriteCanvas = this._tools.imageToSurface(this._sprite);

        var tile = new Surface(w, h);
        tile.drawImageRect( _spriteCanvas.getSurface(), x, y, w, h, 0, 0 );

        var image = new Image();
        image.src = tile.getSurface().toDataURL();
        return image;
    };

//--------------------------------------------------------------------//

    this.flipFrames = function(axis)
    {
        for(var i = 0; i < this._frames.length; i++) {
            var frame = this._frames[i];
            var image = new Image();
            
            var tile = new Surface(frame.width, frame.height);

            tile.flip(frame, axis);
        
            image.src = tile.getSurface().toDataURL();
            _instance._frames[i] = image;
        }
    };
    
//--------------------------------------------------------------------//
	
    this.loadFrame = function(x, y, w, h)
    {
        this._frames.push(this.getTile(x, y, w, h));
    };
	
//--------------------------------------------------------------------//

    this.loadFramesRow = function(x0, y, w, h, xs) 
    {
        for (var x = x0; x < this._sprite.width; x += xs) {
            this.loadFrame(x, y, w, h);
        }
    };
	
//--------------------------------------------------------------------//

    this.loadFrames = function(x0, y0, w, h, xs, ys)
    {
        if (ys > 0) {
            var y = y0;
            for (var y = y0; y < this._sprite.height; y += ys) {				
                if (xs > 0) {
                    this.loadFramesRow(x0, y, w, h, xs);
                } else {
                    this.loadFrame(x0, y, w, h);
                }
            }
        } else if (xs > 0) {
            this.loadFramesRow(x0, y, w, h, xs);
        } else {
            this.loadFrame(x0, y0, w, h); // alias a loadFrame
        }
    };

//--------------------------------------------------------------------//

};
