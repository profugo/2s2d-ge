// Module: Scene handling class
// Version: 1.1
// ES: 3
// Created: xxxx-xx-xx
// Updated: 2020-06-16

/*
 * 20200616: Added getLoadStatus for compatibility
 * 20200323: Various standard complience fixes
 * 20180610: ???
 * 
 */

var Scene = function() {
	
    var _instance = this;
    this._tools = new Toolbox();
    this.reader = typeof window.cordova !== 'undefined' ? new CordovaFileReader() : false;
    this._status = 0;
    this._tileMap;
    this._imgItems;
    this._tiles = [];
    this._items = [];
    this._arrLoadSpool = [];
    this._collisionGrid;

    this._tmp;
//--------------------------------------------------------------------//	

    this.status = function()
    {
        return this._status;
    };

//--------------------------------------------------------------------//

    this.getLoadStatus = function()
    {
        return this._status;
    }
    
//--------------------------------------------------------------------//
	
    this._loadSpool = function(key, value)
    {
        key = key.split('/').slice(-1).pop();

        if (typeof value === 'undefined') {
            var r = false;
            $(this._arrLoadSpool).each(function(e) {
                if (e.key === key) {
                    r = e.target;
                }
            });
            return r;
        } else {
            key = this._tools.MD5(key);
            this._arrLoadSpool.push({
                key : key,
                target : value
            });

        }
    };
	
//--------------------------------------------------------------------//

    this._loadImage = function(src, target)
    {
        if (typeof src === "string") {
            src = [src];
        }

        _instance._target = target;
        $(src).each(function(value) { 
            _instance._loadSpool(value, _instance._target);
            _instance._status += 1;
            _instance._tools.loadImage(src, function(img) {
                var vk = _instance._tools.MD5(img.src.split('/').slice(-1).pop());
                var target = _instance._loadSpool(vk);
                _instance[target] = img;
                _instance._status -= 1;
            });
        });
    };
	
//--------------------------------------------------------------------//

    this.loadTiles = function(src)
    {
        this._loadImage(src, "_tileMap");
    };

//--------------------------------------------------------------------//

    this.loadItems = function(src)
    {
        this._loadImage(src, "_imgItems");
    };
	
//--------------------------------------------------------------------//

    this.loadCollisionMap = function(fileName)
    {
        this._status += 1;

        if (typeof window.cordova !== 'undefined') {
            this.reader.readFromFile("www/" + fileName,
            function(json) {
                _instance._collisionGrid = json;
                _instance._status -= 1;
            });

        } else {
            $.get(fileName,
                function(json) {
                    _instance._collisionGrid = json;
                    _instance._status -= 1;
                },
                'json');
        }
    };

//--------------------------------------------------------------------//
	
    this._getTile = function(x, y, w, h)
    {
        var _tileMapCanvas = this._tools.imageToSurface(this._tileMap);

        var tile = new Surface(w, h);
        tile.drawImageRect( _tileMapCanvas.getSurface(), x, y, w, h, 0, 0 );

        var image = new Image();
        image.src = tile.getSurface().toDataURL();
        return image;
    };

//--------------------------------------------------------------------//

    this.getTiles = function(w, h)
    {
        var yf = this._tileMap.height - h;
        var xf = this._tileMap.width - w;

        for (var y = 0; y <= yf; y += h) {
            for (var x = 0; x <= xf; x += w) {
                this._tiles.push(this._getTile(x, y, w, h));
            }
        }
    };

//--------------------------------------------------------------------//
	
    this._getItem = function(x, y, w, h)
    {
        var _tileMapCanvas = this._tools.imageToSurface(this._imgItems);

        var tile = new Surface(w, h);
        tile.drawImageRect( _tileMapCanvas.getSurface(), x, y, w, h, 0, 0 );

        var image = new Image();
        image.src = tile.getSurface().toDataURL();
        return image;
    };

//--------------------------------------------------------------------//

    this.getItems = function(w, h)
    {
        var yf = this._imgItems.height - h;
        var xf = this._imgItems.width - w;

        for (var y = 0; y <= yf; y += h) {
            for (var x = 0; x <= xf; x += w) {
                this._items.push(this._getItem(x, y, w, h));
            }
        }
    };
	
//--------------------------------------------------------------------//

    this.get = function(tile, item)
    {
        return item === true ? this._items[tile] : this._tiles[tile];
    };

//--------------------------------------------------------------------//

};
