
var GameInit = function()
{

	
// ------------------------------------------------------------------ //

	this._init = function()
	{
		this.stateMachine();
	}
	
// ------------------------------------------------------------------ //


	this.stateMachine = function()
	{
		var intro = {};
		var play  = {};

		GameIntro.call( intro );
		GamePlay.call(  play  );
		
		var state = 0;
		var level = 1;
		var first = 1;
		
		var clock = setInterval( function()
		{
			switch( state )
			{
				case 0: // playing intro
					intro.introLoop();
					if( intro.stopit ) state = 1;
					break;
				case 1:
					play.test();
					state = 2;
					break;
				case 2:
					play.testloop();
					break;
				case 100:
					clearInterval( clock );
					console.log('App terminated succesfully');
					break;
				default:
					console.log('StateMachine Error: '+this.intState);
					break;
			}
		}, 5 ); // 4 times per second
	}
	
// ------------------------------------------------------------------ //

	this._init();
}
