var GamePlay = function()
{

	var LoadMapStatus;
	var GameStatus;
	
	var arrLevelmap;

	var _floorwindow = {};
	
	var inty;
	
	var world  = {};
	var ship   = {};
	var shadow = {};
	var keys   = {};
		
	var scene;	

	
// ------------------------------------------------------------------ //

	this._init = function()
	{
		this._bkgImage = new Image();
		this._floor    = new Image();
		this._tiles    = new Image();
		this._ship     = new Image();
		
		this._bkgImage.src = "img/spacefield_bg1.jpg";
		this._floor.src    = "img/floor1.png";
		this._tiles.src    = "img/blank.gif";
		this._ship.src     = "img/ship.png";
		
		this.LoadMapStatus = 0;
		this.inty          = 96;
		this.framedelta    = ( 1 / 60 );
		this.frameduration = this.framedelta;
		
		this.tools = {};
		
		Toolbox.call( this.tools );		
		this.clock = new this.tools.FrameTimer();
		
		this._bkgImage = this.tools.imageToSurface( this._bkgImage );
		this._tiles    = this.tools.imageToSurface( this._tiles    );
		this._floor    = this.tools.imageToSurface( this._floor    );
		this._ship     = this.tools.imageToSurface( this._ship     );
		
		Sprite.call( ship );
		
		ship.initialize({
			name    : 'MyShip',
			tilemap : { 
				file  : this._ship, 
				width : 48 
			},
			frames  : [
				{ tile : 0, time : .2 },
				{ tile : 1, time : .2 },
				{ tile : 2, time : .2 },
				{ tile : 3, time : .2 },
				{ tile : 5, time : .2 },
				{ tile : 6, time : .2 }
			],
			animations : {
				still : [ 3, 2 ],
				right : [ 1, 0 ],
				left  : [ 4, 5 ]
			},
			animation : {
				name : 'still',
				loop : true
			},
			position : {
				x : 480,
				y : 500
			}
		});
		
		
		Sprite.call( shadow );

		shadow.initialize({
			name    : 'Shadow',
			tilemap : { 
				file  : this._ship, 
				width : 48 
			},
			frames  : [
				{ tile : 7,  time : .2 },
				{ tile : 10, time : .2 },
				{ tile : 13, time : .2 }
			],
			animations : {
				still : [ 1 ],
				right : [ 0 ],
				left  : [ 2 ]
			},
			animation : {
				name : 'still',
				loop : true
			},
			position : {
				x : 488,
				y : 516
			}
		});
				
		this.floorwindow = new Surface( '0', 1104, 672 );
		this.scene       = new Primitives( 'canvas' ,1008, 576 );
	}	
	
// ------------------------------------------------------------------ //
	this.test = function()
	{
		var map = new Array( 14 );
		for ( var y = 0; y <= 13; y++ )
		{
			map[ y ] = new Array( 11 );
			for( var x = 0; x < 10; x++ )
			{
				map[ y ][ x ] = Math.floor( Math.random() * 16 + 1 );
			}
		}
		
		_floorwindow = new Layer_Tiles();
		_floorwindow.initialize( 
		{
			tilemap      : this._floor,
			levelmap     : map,
			name         : 'piso',
			dimensions   : { width       : 480, 
							 height      : 672, 
							 view_width  : 480, 
							 view_height : 576
							},
			position     : { top         : 0,
			                 left        : 240
			               },
			viewposition : { top         : 96,
			                 left        : 0
			               }
		});
		
		_floorwindow.fillTiles();
		
		this.scene.drawImage( this._bkgImage.getCanvas(), 0, 0 );
		//~ this.scene.drawImage( _floorwindow.getViewport().getCanvas(), 240,0 );
						
		this.keys = {};
		Input.call( this.keys );
	}
	
// ------------------------------------------------------------------ //
	
	this.filltop = function()
	{
		var map = _floorwindow.levelMap();
		var row = new Array( 10 );
		
		for( var x = 0; x < 10; x++ )
		{
			row[ x ] = Math.floor( Math.random() * 16 + 1 );
		}
		
		map.unshift( row );
		map.pop();
		
		_floorwindow.levelMap( map );
		_floorwindow.fillTiles();
		_floorwindow.viewPosition( { top : 47, left : 0 } );
	}
	
// ------------------------------------------------------------------ //

	this.testloop = function()
	{
		this.clock.tick();
		this.frameduration -= this.clock.getSeconds();
		if( this.frameduration <=0 )
		{
			this.frameduration = this.framedelta;
			this.scene.drawImage( _floorwindow.getViewport().getCanvas(),
				240,
				0);

			this.scene.drawImage( shadow.getFrame(), shadow.position().x, shadow.position().y );
			this.scene.drawImage( ship.getFrame(), ship.position().x, ship.position().y );
			
			var inty = _floorwindow.viewPosition().top;
			inty = ( inty - 2 );
			if( inty < 0 ) 
			{
				this.filltop();
			} else {
				_floorwindow.viewPosition( { top : inty, left : 0 } );
			}
		
			//this.scene.text( 'bold 10pt Arial', '#fff', 252, 16, inty );
			
			if( this.keys.KeyStatus( 'right' ) == 1 )
			{
				var px = ( ship.position().x + 4 );
				ship.position( { x: px, y : 500 } );
				shadow.position( { x: ( px + 8 ), y : 516 } );
			};
			if( this.keys.KeyStatus( 'left'  ) == 1 )
			{
				var px = ( ship.position().x - 4 );
				ship.position( { x: px, y : 500 } );
				shadow.position( { x: ( px + 8 ), y : 516 } );
			};
		
		}
		var p = 'still';
		if( this.keys.KeyStatus( 'right' ) == 1 ) p = 'right';
		if( this.keys.KeyStatus( 'left'  ) == 1 ) p = 'left';
		if( ship.currentAnimation() != p)
		{
			ship.currentAnimation( p, true);
			shadow.currentAnimation( p, true);
		}
		ship.tic( this.clock.getSeconds() );
		shadow.tic( this.clock.getSeconds() );
		if( ship.eoa() == true )
		{
			switch ( ship.currentAnimation() )
			{
				case 'still':
					ship.currentAnimation( 'left' );
					shadow.currentAnimation( 'left' );
					break;
				case 'left':
					ship.currentAnimation( 'still' );
					shadow.currentAnimation( 'still' );
					break;
			}
		}
	}
	
// ------------------------------------------------------------------ //

	
	this.clearLevelMap = function()
	{
		this.arrLevelMap = new Array( 128 );
		for(var i = 0; i < 128; i++ ){
			this.arrLevelMap[ i ] = new Array( 64 );
			for( var j = 0; j < 64; j++ ){
				this.arrLevelMap[ i ][ j ] = 0;
			}
		}
	}

// ------------------------------------------------------------------ //

	this.loadMap = function( level )
	{
		this.LoadMapStatus = 1;
		this.clearLevelMap();
		var line = eval( '(' + level + ')' );
		for( var i = 0; i < line.length; i++ )
		{
			for( var j = 0; j < line[ i ].row.length; j++ )
			{
				this.arrLevelMap[ j ][ i ] = line[ i ].row[ j ];
			}
		}
		
		this.LoadMapStatus = 2;

	}
	
// ------------------------------------------------------------------ //

	this.initGame = function()
	{
		World.call( world, this._bkgImage, this._tiles, this.arrLevelMap );
		world.expandCollitionMap( collitionMaps );
		Snake.call( snake, 480, 216, this._bullet, world, this.dificulty );
		this.keys = {};
		Input.call( this.keys );
		
		this.GameStatus    = 0;	
		this.LoadMapStatus = 0;
		
		this.setFruitPosition();
		snake.setDirection( 2 );
	}
	
// ------------------------------------------------------------------ //
	
	this.setFruitPosition = function()
	{
		var nookpos = true;
		while( nookpos )
		{
			this.fruitx = Math.floor( Math.random() * 40 ) * 24 + 24;
			this.fruity = Math.floor( Math.random() * 22 ) * 24 + 24;
			nookpos = world.checkCollition( Array(
						Array( this.fruitx, this.fruity )
						) );
		}
	}
	
// ------------------------------------------------------------------ //
	
	this.getCursorKeys = function()
	{
		if( this.keys.KeyStatus( 'up'    ) == 1 ) return 1;
		if( this.keys.KeyStatus( 'right' ) == 1 ) return 2;
		if( this.keys.KeyStatus( 'down'  ) == 1 ) return 3;
		if( this.keys.KeyStatus( 'left'  ) == 1 ) return 4;
	}
	
// ------------------------------------------------------------------ //
	
	this.checkCollitions = function()
	{
		return ( snake.checkOutofBounds() || snake.checkSelfCollition() || snake.checkHeadCollition() );
	}
	
// ------------------------------------------------------------------ //

	this.renderFrame = function()
	{
		var s = world.getViewport( 0, 0 );
		
		this.scene.drawImage( s.getCanvas(), 0, 0 );
		snake.renderSnake( this.scene );
		for( var x=0; x<this.fruits;x++)
		{
			this.scene.drawImage( this._prize.getCanvas(), x * 24, 0 );
		}
		this.scene.drawImage( this._prize.getCanvas(), this.fruitx, this.fruity );
	}
	
// ------------------------------------------------------------------ //


	this.doGameLoop = function()
	{
		var dir = this.getCursorKeys();
		if( dir && snake.turnto == 0) snake.setDirection( dir );
		snake.updatePosition();
		if( snake.eatFruit( this.fruitx, this.fruity ) == true )
		{
			this.fruits--;
			if( this.fruits < 1 ){
				this.GameStatus = 98;
				this.keys = null;
			} else {
				this.setFruitPosition();
			}
		}

		if(  this.checkCollitions() == true ) 
		{
			this.GameStatus = 99;
			this.keys = null;
		} else {
			this.renderFrame();
		}
	}
		
// ------------------------------------------------------------------ //


	this._init();
	
// ------------------------------------------------------------------ //

}
