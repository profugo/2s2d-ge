var GameIntro = function()
{
	var escenario;
	var pantalla;
	var teclas = {};
	var stopit;
	var dummymap;
	var target;
	var titleship;
	var starfld;
	var starcolors;
	var fullscreen;
	var tb  = {};
	
// ------------------------------------------------------------------ //

	this._init = function()
	{
		var imf = new Image();
		var imt = new Image();
		var ims = new Image();

		this.framedelta    = ( 1 / 60 );
		this.frameduration = this.framedelta;
		
		Toolbox.call( tb );
		this.clock = new tb.FrameTimer();
		
		imf.src = "img/spacebg.jpg";
		imt.src = "img/blank.gif";
		ims.src = "img/050517-1_img.png";
		
		var f = tb.imageToSurface( imf );
		var t = tb.imageToSurface( imt );
		this.titleship = tb.imageToSurface ( ims );
		this.teclas = {};
		Input.call( this.teclas );
		
		this.dummymap   = new Array( 128 );
		this.stopit     = false;
		this.starcolors = [ '#fff', '#ddd', '#bbb', '#999', '#777', '#555', '#333', '#111' ];
		this.fullscreen = 0;
		
		for(var i = 0; i < 128; i++ ){
			this.dummymap[ i ] = new Array( 64 );
			for( var j = 0; j < 64; j++ ){
				this.dummymap[ i ][ j ] = 0;
			}
		}

		this.starfld = new Array( 100 );
		for( var i = 0; i < 100; i++ )
		{
			this.starfld[ i ] =[ Math.floor( Math.random() * 1008 - 504 ) ,
								 Math.floor( Math.random() * 576 - 288 ),
								 Math.floor( Math.random() * 582 ) 
								];
		}
		this.escenario = new World( f, t, this.dummymap );
		this.pantalla = new Primitives( 'canvas' ,1008, 576 );
	}
	
// ------------------------------------------------------------------ //
	this.captureKeyboard = function()
	{
		this.teclas = new Input();
	}
	
// ------------------------------------------------------------------ //

	this.flat3dCoords = function( x, y, z )
	{
		return [ Math.floor( x * 500 / z ), Math.floor( y * 500 / z ) ];
		
	}
	
// ------------------------------------------------------------------ //

	this.updateStars = function()
	{
		for( var i = 0; i < 100; i++ )
		{
			var s2d = this.flat3dCoords( this.starfld[ i ][ 0 ] , this.starfld[ i ][ 1 ], this.starfld[ i ][ 2 ] );
			if( s2d[ 0 ] >= -504 && s2d[ 0 ] <= 504 && s2d[ 1 ] >= -288 && s2d[ 1 ] <= 288  && this.starfld[ i ][ 2 ] > 0 )
			{
				var colour    = ( Math.floor( this.starfld[ i ][ 2 ] / 73 ) );
				var starwidth = ( 3 - Math.floor( this.starfld[ i ][ 2 ] / 194 ) ) ;
				this.pantalla.pset( s2d[ 0 ] + 504, s2d[ 1 ] + 288, starwidth, this.starcolors[ colour ] );
				this.starfld[ i ][ 2 ] = this.starfld[ i ][ 2 ] - 4;
			} else {
				this.starfld[ i ] = [ Math.floor( Math.random() * 1008 - 504 ) ,
									 Math.floor( Math.random() * 576 - 288 ),
									 582 ];
			}
		}
	}
	
// ------------------------------------------------------------------ //


	this.introLoop = function()
	{

		this.clock.tick();
		this.frameduration -= this.clock.getSeconds();
		//console.log( this.frameduration +', '+this.clock.getSeconds());
		if( this.frameduration <=0 )
		{
			this.frameduration = this.framedelta;
			var s = this.escenario.getViewport( 0, 0 );
			this.pantalla.drawImage( s.getCanvas(), 0, 0 );
			this.updateStars();
			this.pantalla.drawImage( this.titleship.getCanvas(), 710, -10 );
			
			var fuente = " 14pt Stalin One";
			var ftitle = "bold 48pt Stalin One";
			var color  = "#fff";
			
			var lblx = this.pantalla.textSize( ftitle, 'Kessel Run' );
			this.pantalla.text( ftitle, '#000', (712 - lblx[ 0 ] ), 291, 'Kessel Run' );
			this.pantalla.text( ftitle, '#FF0', (710 - lblx[ 0 ] ), 289, 'Kessel Run' );

			var lblx = this.pantalla.textSize( fuente, 'Press "Space" to start...M' );
			
			this.pantalla.text( fuente, '#000', (1008 - lblx[0]),561,'Press "Space" to start...');
			this.pantalla.text( fuente, color, (1008 - lblx[0]),559,'Press "Space" to start...');
		}
		if( this.teclas.KeyStatus( 'fire1' ) == 1 ) this.stopit = true;
		
	}; 

// ------------------------------------------------------------------ //

	this._init();
}
