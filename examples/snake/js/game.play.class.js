var GamePlay = function()
{

    var LoadMapStatus;
    var GameStatus;

    var arrLevelmap;

    // Graficos del game
    var _bkgImage;
    var _bullet;
    var _prize;
    var _tiles;

    var world = {};
    var snake = {};
    var keys  = {};

    var scene;	


    var fruitx;
    var fruity;
    var fruits;
    var dificulty;

// ------------------------------------------------------------------ //

    this._init = function()
    {
        this._bkgImage = new Image();
        this._bullet   = new Image();
        this._prize    = new Image();
        this._tiles    = new Image();

        this._bkgImage.src = "img/oceano.jpg";
        this._bullet.src   = "img/bullet.png";
        this._prize.src    = "img/prize.png";
        this._tiles.src    = "img/tiles.png";

        this.LoadMapStatus = 0;

        var tools = {};
        Toolbox.call( tools );


        this._bkgImage = tools.imageToSurface( this._bkgImage );
        this._tiles    = tools.imageToSurface( this._tiles    );
        this._bullet   = tools.imageToSurface( this._bullet   );
        this._prize    = tools.imageToSurface( this._prize    );
        this.scene     = new Primitives( 'canvas' ,1008, 576 );

    };	

// ------------------------------------------------------------------ //
	
    this.clearLevelMap = function()
    {
        this.arrLevelMap = new Array( 128 );
        for(var i = 0; i < 128; i++ ){
            this.arrLevelMap[ i ] = new Array( 64 );
            for( var j = 0; j < 64; j++ ){
                this.arrLevelMap[ i ][ j ] = 0;
            }
        }
    };

// ------------------------------------------------------------------ //

    this.loadMap = function( level )
    {
        this.LoadMapStatus = 1;
        this.clearLevelMap();
        var line = eval( '(' + level + ')' );
        for( let i = 0, l = line.length; i < l; i++ )
        {
            for( let j = 0, m = line[ i ].row.length; j < m; j++ )
            {
                this.arrLevelMap[ j ][ i ] = line[ i ].row[ j ];
            }
        }

        this.LoadMapStatus = 2;

    };

// ------------------------------------------------------------------ //

    this.initGame = function()
    {
        World.call( world, this._bkgImage, this._tiles, this.arrLevelMap );
        world.expandCollitionMap( collitionMaps );
        Snake.call( snake, 480, 216, this._bullet, world, this.dificulty );
        this.keys = {};
        Input.call( this.keys );

        this.GameStatus    = 0;	
        this.LoadMapStatus = 0;

        this.setFruitPosition();
        snake.setDirection( 2 );
    };
	
// ------------------------------------------------------------------ //
	
    this.setFruitPosition = function()
    {
        var nookpos = true;
        while( nookpos )
        {
            this.fruitx = Math.floor( Math.random() * 40 ) * 24 + 24;
            this.fruity = Math.floor( Math.random() * 22 ) * 24 + 24;
            nookpos = world.checkCollition([[this.fruitx, this.fruity ]]);
        }
    };
	
// ------------------------------------------------------------------ //
	
    this.getCursorKeys = function()
    {
        if( this.keys.KeyStatus( 'up'    ) === 1 ) return 1;
        if( this.keys.KeyStatus( 'right' ) === 1 ) return 2;
        if( this.keys.KeyStatus( 'down'  ) === 1 ) return 3;
        if( this.keys.KeyStatus( 'left'  ) === 1 ) return 4;
    };
	
// ------------------------------------------------------------------ //
	
    this.checkCollitions = function()
    {
        return ( snake.checkOutofBounds() || snake.checkSelfCollition() || snake.checkHeadCollition() );
    };
	
// ------------------------------------------------------------------ //

    this.renderFrame = function()
    {
        var s = world.getViewport( 0, 0 );

        this.scene.drawImage( s.getCanvas(), 0, 0 );
        snake.renderSnake( this.scene );
        for( var x=0; x<this.fruits;x++)
        {
            this.scene.drawImage( this._prize.getCanvas(), x * 24, 0 );
        }
        this.scene.drawImage( this._prize.getCanvas(), this.fruitx, this.fruity );
    };
	
// ------------------------------------------------------------------ //

    this.doGameLoop = function()
    {
        var dir = this.getCursorKeys();
        if( dir && snake.turnto === 0) snake.setDirection( dir );
        snake.updatePosition();
        if( snake.eatFruit( this.fruitx, this.fruity ) === true )
        {
            this.fruits--;
            if( this.fruits < 1 ){
                this.GameStatus = 98;
                this.keys = null;
            } else {
                this.setFruitPosition();
            }
        }

        if(  this.checkCollitions() === true ) 
        {
            this.GameStatus = 99;
            this.keys = null;
        } else {
            this.renderFrame();
        }
    };

// ------------------------------------------------------------------ //

    this._init();
	
// ------------------------------------------------------------------ //

};
