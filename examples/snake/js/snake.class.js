var Snake = function(intX, intY, imgBullet, world, dificulty )
{
    var segments;
    var addSegments;
    var sprite;
    var speed;
    var direction;
    var turnto;
    var world;
    var increment;
	
// ------------------------------------------------------------------ //

    this._init = function( intX, intY, imgBullet, world, dificulty )
    {
        this.sprite = imgBullet;
        this.world = world;
        switch ( dificulty )
        {
            case 0:
                this.speed = 12;
                this.addSegments = 4;
                break;
            case 1:
                this.speed = 12;
                this.addSegments = 7;
                break;
            case 2:
                this.speed = 12;
                this.addSegments = 10;
                break;
            case 3:
                this.speed = 12;
                this.addSegments = 15;
                break;
            case 4:
                this.speed = 24;
                this.addSegments = 8;
                break;
        }

        this.increment = this.addSegments;
        this.direction = 2;
        this.turnto    = 0;
        this.segments  = new Array( new Array( intX, intY ) );
    };

// ------------------------------------------------------------------ //
		
    this.checkOutofBounds = function()
    {
        var ret = false;
        var posX = this.segments[ 0 ][ 0 ];
        var posY = this.segments[ 0 ][ 1 ];

        if( posX < 0 || posY < 0 || posX > 980 || posY > 550 ) ret = true;

        return ret;
    };

// ------------------------------------------------------------------ //
	
    this.checkSelfCollition = function()
    {
        var ret  = false;
        var posX = this.segments[ 0 ][ 0 ];
        var posY = this.segments[ 0 ][ 1 ];

        for(let i = 6, l = this.segments.length; i < l; i++ )
        {
            if(posX >= this.segments[ i ][ 0 ] && posX <= (this.segments[ i ][ 0 ] + 23) &&
               posY >= this.segments[ i ][ 1 ] && posY <= (this.segments[ i ][ 1 ] + 23) ) ret = true;
        }
        return ret;
    };

// ------------------------------------------------------------------ //

    this.checkHeadCollition = function()
    {
            var posX = this.segments[ 0 ][ 0 ];
            var posY = this.segments[ 0 ][ 1 ];

            return this.world.checkCollition(
                new Array(
                 [ posX, posY ],
                [ posX + 23, posY ],
                [ posX, posY + 23 ],
                [ posX + 23, posY + 23 ]
            ));
    };
	
// ------------------------------------------------------------------ //

    this.getHeadPosition = function()
    {
        return new Array( this.segments[ 0 ][ 0 ], this.segments[ 0 ][ 1 ] );
    };
	
// ------------------------------------------------------------------ //
	
    this.setDirection = function( intDirection )
    {
        if( ( intDirection === 1 && this.direction === 3 ) ||
            ( intDirection === 2 && this.direction === 4 ) ||
            ( intDirection === 3 && this.direction === 1 ) ||
            ( intDirection === 4 && this.direction === 2 ) )
        {
            this.turnto=0;
            return false;
        }

        if( this.segments[ 0 ][ 0 ] % 24 === 0 && this.segments[ 0 ][ 1 ] % 24 === 0) 
        {
            this.direction = (this.turnto > 0 ? this.turnto : intDirection );
            this.turnto = 0;
        } else {
            if( this.turnto === 0 ) this.turnto = intDirection;
        }
    };

// ------------------------------------------------------------------ //
	
    this.eatFruit = function( posX, posY )
    {
        if( posX >= this.segments[ 0 ][ 0 ] && posX <= (this.segments[ 0 ][ 0 ] + 23) &&
            posY >= this.segments[ 0 ][ 1 ] && posY <= (this.segments[ 0 ][ 1 ] + 23) )
        {
            this.addSegments = this.increment;
            return true;
        }
        return false;
    };

// ------------------------------------------------------------------ //

    this.renderSnake = function( surWhere )
    {
        for( var i = ( this.segments.length - 1); i >= 0; i-- )
        {
            surWhere.drawImage( this.sprite.getCanvas(), this.segments[ i ][ 0 ], this.segments[ i ][ 1 ] );
        }
    };

// ------------------------------------------------------------------ //
		
    this.updatePosition = function()
    {
        var last  = ( this.segments.length - 1 );
        var lastX = this.segments[ last ][ 0 ];
        var lastY = this.segments[ last ][ 1 ];
        var speedX = (this.direction === 2 || this.direction === 4 ? ( this.direction === 2 ? this.speed: - ( this.speed ) ) : 0 );
        var speedY = (this.direction === 1 || this.direction === 3 ? ( this.direction === 3 ? this.speed: - ( this.speed ) ) : 0 );
        this.segments[ 0 ][ 0 ] = ( this.segments[ 0 ][ 0 ] + speedX );
        this.segments[ 0 ][ 1 ] = ( this.segments[ 0 ][ 1 ] + speedY );
        for( var i = last; i > 0; i--)
        {
            this.segments[ i ][ 0 ] = this.segments[ i - 1 ][ 0 ];
            this.segments[ i ][ 1 ] = this.segments[ i - 1 ][ 1 ];
        }

        if(this.turnto > 0) this.setDirection(this.turnto);

        if(this.addSegments > 0)
        {
            this.segments.push( new Array( lastX, lastY ) );
            this.addSegments--;
        }
    };
	
// ------------------------------------------------------------------ //
	
    this._init( intX, intY, imgBullet, world, dificulty );
};
