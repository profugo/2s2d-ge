
var GameInit = function()
{

    var lives     = 5;
    var score     = 0;
    var lastlvl   = 6;
    var dificulty = 2;
	
// ------------------------------------------------------------------ //

    this._init = function()
    {
        this.setDificulty();
        this.stateMachine();
    };
	
// ------------------------------------------------------------------ //
	
    this.setDificulty = function()
    {
        var targets = new Array( 5, 10, 15, 20, 20 );
        target = targets[ dificulty ];
    };
	
// ------------------------------------------------------------------ //


    this.stateMachine = function()
    {
        var intro = {};
        var play  = {};

        GameIntro.call( intro );
        GamePlay.call(  play  );

        var state = 0;
        var level = 1;
        var first = 1;

        var clock = setInterval( function()
        {
            switch( state )
            {
                case 0: // playing intro
                    intro.introLoop();
                    if( intro.stopit ) state = 1;
                    break;
                case 1: // set dificulty

                    intro.setDificulty( dificulty, first );
                    first = 0;
                    dificulty = intro.dificulty;
                    if( intro.stopit ) 
                    {
                        state = 10;
                        var targets = new Array( 5, 10, 15, 20, 20 );
                        target = targets[ dificulty ];
                        first = 1;
                    }
                    break;
                case 10: // load level map
                    switch( play.LoadMapStatus )
                    {
                        case 0: // load level map
                            var l = (level <= lastlvl ? level : lastlvl );
                            play.loadMap( Levels[ l ] );
                            break;
                        case 1: // loading (shouldn't be seen, this should happen between clock ticks)
                            // Still loading? damn!
                            break;
                        case 2: // load finished set state to init game
                            state = 11;
                            intro.levelStartPause( level, target );
                            break;
                    }
                    break;
                case 11:
                    intro.levelStartPause( level );
                    if( intro.stopit ) state = 12;
                        break;
                case 12:	// initialize level
                    play.dificulty = dificulty;
                    play.fruits    = target;
                    play.initGame();
                    state = 13;
                    break;
                case 13: // playing game
                    play.doGameLoop();
                    if( play.GameStatus == 99 ) 
                    {
                        state = 99;
                        intro.gameOver( 1 );
                    }
                    if( play.GameStatus == 98 )
                    {
                        level++;
                        intro.levelComplete( 1 );
                        state = 14;
                    }
                    break;
                case 14:
                    intro.levelComplete();
                    if( intro.stopit ) state = 10;
                    break;
                case 99:
                    play.renderFrame();
                    intro.gameOver();
                    if( intro.stopit ) 
                    {
                        level = 1;
                        state = 0;
                        intro.stopit = 0;
                    }
                    break;
                case 100:
                    clearInterval( clock );
                    console.log('App terminated succesfully');
                    break;
                default:
                    console.log('StateMachine Error: '+this.intState);
                    break;
            }
        }, 50 );
    };
	
// ------------------------------------------------------------------ //

    this._init();
};
