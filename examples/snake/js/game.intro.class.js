var GameIntro = function()
{
    var d2r;
    var inty;
    var intcontador;
    var escenario;
    var pantalla;
    var teclas = {};
    var stopit;
    var dummymap;
    var target;
    var dificulty;

// ------------------------------------------------------------------ //

    this._init = function()
    {
        var imf = new Image();
        var imt = new Image();
        var tb  = new Toolbox();

        imf.src = "img/oceano.jpg";
        imt.src = "img/tiles.png";

        var f = tb.imageToSurface( imf );
        var t = tb.imageToSurface( imt );
        this.teclas = {};
//        Input.call( this.teclas );
        this.dummymap = new Array( 128 );
        this.stopit   = false;

        for(var i = 0; i < 128; i++ ){
            this.dummymap[ i ] = new Array( 64 );
            for( var j = 0; j < 64; j++ ){
                this.dummymap[ i ][ j ] = 0;
            }
        }

        this.inty = 0;
        this.d2r  = Math.PI / 180;
        this.intcontador = 0;
        this.escenario = new World( f, t, this.dummymap );
        this.pantalla = new Primitives( 'canvas' ,1008, 576 );
    };
	
// ------------------------------------------------------------------ //
    this.captureKeyboard = function()
    {
        this.teclas = new Input();
    };
	
// ------------------------------------------------------------------ //

    this.introLoop = function()
    {
        this.intcontador++;
        if( this.intcontador === 360 )this.intcontador = 0;

        this.inty = Math.floor( Math.sin( this.d2r * ( this.intcontador ) ) * 192 ) + 192;

        var s = this.escenario.getViewport( 0, this.inty );
        this.pantalla.drawImage( s.getCanvas(), 0, 0 );

        var fuente = " 24pt Mystery Quest";
        var ftitle = "bold 48pt Mystery Quest";
        var color  = "#fff";

        var lblx = this.pantalla.textSize( ftitle, 'SNAKE GAME' );
        this.pantalla.text( ftitle, '#000', (506 - lblx[ 0 ] / 2 ), 291, 'SNAKE GAME' );
        this.pantalla.text( ftitle, '#FF0', (504 - lblx[ 0 ] / 2 ), 289, 'SNAKE GAME' );

        this.pantalla.text( fuente, '#000', 700,561,'Press "Space" to start...');
        this.pantalla.text( fuente, color, 700,559,'Press "Space" to start...');

        if( this.teclas.KeyStatus( 'fire1' ) === 1 ) this.stopit = true;
    };

// ------------------------------------------------------------------ //

    this.levelStartPause = function( level, target )
    {
            if( target )
            { 
                this.intcontador = 5; 
                this.inty        = 0;
                this.stopit      = false;
                this.target      = target;
            } else {
                this.inty++;
                if( this.inty === 20 )
                {
                    this.intcontador--;
                    this.inty = 0;
                    if( this.intcontador === -1 )
                    { 
                        this.stopit = true; 
                    }
                }
            }

            var s = this.escenario.getViewport( 0, 0 );
            this.pantalla.drawImage( s.getCanvas(), 0, 0 );

            var fuente = " 24pt Mystery Quest";
            var ftitle = "bold 48pt Mystery Quest";

            var lvlx = this.pantalla.textSize(ftitle, 'LEVEL ' + level );
            var advx = this.pantalla.textSize(fuente, 'Collect ' + this.target + ' gems to advance to the next level' );
            var conx = this.pantalla.textSize(ftitle, this.intcontador );

            this.pantalla.text( ftitle, '#000', ( 506 - lvlx[ 0 ] / 2 ), 220, 'LEVEL ' + level );
            this.pantalla.text( ftitle, '#4f0', ( 504 - lvlx[ 0 ] / 2 ), 218, 'LEVEL ' + level );

            this.pantalla.text( fuente, '#000', ( 506 - advx[ 0 ] / 2 ),380,'Collect ' + this.target + ' gems to advance to the next level' );
            this.pantalla.text( fuente, '#fff', ( 504 - advx[ 0 ] / 2 ),378,'Collect ' + this.target + ' gems to advance to the next level' );

            if( this.intcontador >= 0 )
            {
                this.pantalla.text( ftitle, '#000', ( 506 - conx[ 0 ] / 2 ), 300, this.intcontador );
                this.pantalla.text( ftitle, '#FF0', ( 504 - conx[ 0 ] / 2 ), 298, this.intcontador );
            }
    };

// ------------------------------------------------------------------ //
	
    this.setDificulty = function( target, first )
    {
        var textsize     = this.pantalla.textSize( "bold 48pt Mystery Quest", "Dificulty: <*****>" );
        var titsize      = this.pantalla.textSize( "bold 32pt Mystery Quest", "Dificulty:." );
        var starswidth   = this.pantalla.textSize( "bold 48pt Mystery Quest", "< *****" );
        var gtsize       = this.pantalla.textSize( "bold 48pt Mystery Quest", ">" );

        var text1x = (504 - textsize[ 0 ] / 2 );
        var text2x = ( text1x + titsize[ 0 ] );
        var text3x = ( text2x + (starswidth[ 0 ] ) );
        if( first === 1 )
        { 
            this.inty        = 0;
            this.stopit      = false;
            this.dificulty   = target;
            this.intcontador = 0;
            this.teclas.clearBuffer();
        } else {
            this.inty++;
            if( this.intcontador === 1 ) this.intcontador = this.teclas.KeyStatus( 'fire1' );
            if( (this.inty >= 2) && (this.intcontador === 0) )
            {
                this.inty = 0;
                if( this.teclas.KeyStatus( 'left'  ) === 1 ) this.dificulty--;
                if( this.teclas.KeyStatus( 'right' ) === 1 ) this.dificulty++;
                if( this.teclas.KeyStatus( 'fire1' ) === 1 ) this.stopit = true;
                this.dificulty = (this.dificulty < 0 ? 0 : this.dificulty > 4 ? 4 : this.dificulty );
            }
        }

        var s = this.escenario.getViewport( 0, 0 );
        this.pantalla.drawImage( s.getCanvas(), 0, 0 );

        //this.pantalla.text( "12pt Arial", '#fff', 20,20,'contador: ' + this.intcontador );

        var ftitle = "bold 32pt Mystery Quest";
        var fstars = "bold 48pt Mystery Quest";

        this.pantalla.text( ftitle, '#000', ( text1x + 2 ), 291, 'Dificulty:' );
        this.pantalla.text( ftitle, '#FF0', text1x, 289, 'Dificulty:' );

        this.pantalla.text( fstars, '#000', ( text2x + 2 ), 302, '<' );
        this.pantalla.text( fstars, '#fff', text2x, 300, '<' );

        this.pantalla.text( fstars, '#000', ( text3x + 2 ), 302, '>' );
        this.pantalla.text( fstars, '#fff', text3x, 300, '>' );

        var dist = ( text2x + gtsize[ 0 ] );
        for(var i = 0; i <= this.dificulty; i++)
        {
            this.pantalla.text( fstars, '#000', dist + 2 + 32 * i, 316, '*' );
            this.pantalla.text( fstars, '#0f0', dist + 32 * i, 314, '*' );
        }

        var fuente = " 24pt Mystery Quest";
        this.pantalla.text( fuente, '#000', 700,561,'Press "Space" to start...');
        this.pantalla.text( fuente, '#fff', 700,559,'Press "Space" to start...');		
    };
	
// ------------------------------------------------------------------ //
	
    this.gameOver = function( target )
    {
        if( target )
        { 
            this.intcontador = 10; 
            this.inty        = 0;
            this.stopit      = false;
            this.captureKeyboard();
        } else {
            this.inty++;
            if( this.inty === 20 )
            {
                this.intcontador--;
                this.inty = 0;
                if( this.intcontador === -1 )
                { 
                    this.stopit = true; 
                }
            }
        }

        var fuente = " 24pt Mystery Quest";
        var ftitle = "bold 48pt Mystery Quest";
        var color  = "#fff";

        var lblx = this.pantalla.textSize( ftitle, 'GAME OVER' );
        this.pantalla.text( ftitle, '#000', ( 506 - lblx[ 0 ] / 2), 291, 'GAME OVER' );
        this.pantalla.text( ftitle, '#FF0', ( 504 - lblx[ 0 ] / 2), 289, 'GAME OVER' );

        var conx = this.pantalla.textSize( ftitle, this.intcontador );
        if( this.intcontador >= 0 )
        {
            this.pantalla.text( ftitle, '#000', (506 - conx[ 0 ] / 2 ), 380, this.intcontador );
            this.pantalla.text( ftitle, '#fff', (504 - conx[ 0 ] / 2 ), 378, this.intcontador );
        }
    };
	
// ------------------------------------------------------------------ //	
    this.levelComplete = function( target )
    {
        if( target )
        { 
            this.intcontador = 3; 
            this.inty        = 0;
            this.stopit      = false;
            var fuente = " 24pt Mystery Quest";
            var ftitle = "bold 48pt Mystery Quest";
            var color  = "#fff";

            var lvlx = this.pantalla.textSize( ftitle, 'Level Complete!' );

            this.pantalla.text( ftitle, '#000', (506 - lvlx[ 0 ] / 2 ), 291, 'Level Complete!');
            this.pantalla.text( ftitle, '#FF0', (504 - lvlx[ 0 ] / 2 ), 289, 'Level Complete!');

        } else {
            this.inty++;
            if( this.inty === 20 )
            {
                this.intcontador--;
                this.inty = 0;
                if( this.intcontador === -1 )
                { 
                    this.stopit = true; 
                }
            }
        }
    };

// ------------------------------------------------------------------ //

    this._init();
};
